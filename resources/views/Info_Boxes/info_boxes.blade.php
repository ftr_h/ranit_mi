
    <div class="row" class="text-center">
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
              <i class="material-icons">event_available</i>
            </div>
            <p class="card-category">TOTAL REPORT</p>
            <h3 class="card-title" id="biaya_tol"></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">event_available</i> 
              
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
              <i class="material-icons">event_available</i>
            </div>
            <p class="card-category">REPORT FIXED</p>
            <h3 class="card-title" id="biaya_tol"></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">event_available</i> 
              
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
              <i class="material-icons">event_available</i>
            </div>
            <p class="card-category">REPORT UNFIXED</p>
            <h3 class="card-title" id="biaya_tol"></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">event_available</i> 
              
            </div>
          </div>
        </div>
      </div>
    </div>