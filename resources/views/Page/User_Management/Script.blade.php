<style>
    .panel{
        
    }
    .small-box{
        background-color: #1f4e79;
        color: aliceblue;
    
    }
    
    .col-md-2{
    background:#1f4e79;
    color:#FFF;
    margin-bottom: 1%;
    border-radius: 7.5%;      
    }
    .col-half-offset{
        margin-left:2.7%
    
    }
    .small-box p{
        font-size: 11px !important;
    }
    .table-responsive{
        min-height: .01%;
        overflow-x: hidden !important;
    }
    
    @media only screen and (max-width: 600px){
        .table-responsive{
            min-height: .01%;
            overflow-x: scroll !important;
        }
    }
    
    @media only screen and (max-width: 30px){
        .col-xs-5{
            width: 44.666667%;
        }
    }
    
</style>
<script>
$(document).ready(function() {
    $('#id_User_tab').hide();
    $('#table').DataTable({
        responsive: true,
        info: false
    });
    // $('.selectpicker').selectpicker();


 $.ajax({
    url    : "admin/user",
    method : "GET",
    type   : "json",
    success: function (data) {      
        console.log(data);
        // alert(data.page_title);
        var role_user = `<option value="">-- Role --</option>`;
        $.each(data.role,function(index,StObj){
            role_user += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.user_role').html(role_user).selectpicker('refresh');
    }  
  })

    var dataTable = $('#table_user').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url : 'user-datatable',
                method: 'POST',
                data: {
                    "table"     : "ms_user"
                },
                global:false,
                async:false
            },
            
            columns: [
                { data: 'id', name: 'id'},
                { data: 'name', name: 'name'},
                { data: 'username', name: 'username'},
                { data: 'email', name: 'email'},
                { data: 'role_name', name: 'role_name'},
                { data: 'id', render: function (data, type, row, meta) {
                        return `
                        <button onclick="get_data(`+data+`)" type="button" class="btn btn-link btn-just-icon btn-primary btn-edit" data-original-title="Edit Data" title="Edit Data">
                            <i class="material-icons">edit</i>
                        </button>
                        <a class="btn btn-link btn-just-icon btn-danger btn-delete" title="Delete" href="javascript:void(0);" onclick="swal({   
                        title: 'Are you sure to delete this User ?',   
                        // text: 'You will not be able to recover this record data!',   
                        type: 'warning',   
                            showCancelButton: true,   
                            confirmButtonText: 'Yes!', 
                            confirmButtonClass: 'btn btn-success',
                            cancelButtonClass: 'btn btn-danger',
                            cancelButtonText: 'No',  
                            closeOnConfirm: false,
                            buttonsStyling: false
                        }).then (function (result) {

                        if (result.hasOwnProperty('dismiss')) {
                          return;
                        }else{
                          delete_user(${data});
                        }

                        });"><i class="fa fa-trash"></i></a>`;
                    }
                },

            ]
        });
  
} );

$('#bt_add_user').click(function(){
    $('#form-add-user').ajaxSubmit({
        url    : "admin/user/add/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            alert("okokok");
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "UserManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
        }
    });
});

$('#bt_edit_user').click(function(){
    $('#form-edit-user').ajaxSubmit({
        url    : "admin/user/edit/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            alert("okokok");
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "UserManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
        }
    });
});
function delete_user(id) {
    
    $.ajax({
        url    : "admin/user/delete/"+id,
        method : "GET",
        type   : "json",
        success: function (data) {      
            console.log(data);
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "UserManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
          
          // $('#edit_liter').val(data.ajax_po.liter)      
        }  
    })
}
function get_data(id) {
$('#myTab li:nth-child(3) a').tab('show') // Select third tab
$('#id_User_tab').show();
 $.ajax({
    url    : "{{route('ajaxEditUser')}}",
    method : "GET",
    data   : {
                id : id
            },
    type   : "json",
    success: function (data) {      
      console.log(data);
      $('#edit_name').val(data.ajax_user.name).trigger("change");
      $('#edit_username').val(data.ajax_user.username).trigger("change");
      $('#edit_id').val(data.ajax_user.id);
      $('#edit_email').val(data.ajax_user.email).trigger("change");
      $('#edit_password').val("").trigger("change");
      $('#edit_role').selectpicker();
      $('#edit_role').selectpicker('val', data.ajax_user.id_role);
      
      // $('#edit_liter').val(data.ajax_po.liter)      
    }  
  })
}
</script>
