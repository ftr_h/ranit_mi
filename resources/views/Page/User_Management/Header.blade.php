<!-- Your custom  HTML goes here -->
<style type="text/css">
    th{
        text-align: center;
        font-size:14px;
    }
    td{
        font-size:14px;
    }
</style>
<style>
    
    .tab-space {
    padding: 20px 0 0px 0px;
}
    .col-form-label{
        padding: 10px 5px 0 0 !important;
        text-align: right;
    }.form-inline .bootstrap-select, .form-horizontal .bootstrap-select, .form-group .bootstrap-select {
        margin-bottom: 0;
        padding: 0px !important;
    }
    .bootstrap-select .btn.dropdown-toggle.select-with-transition {
        padding-left: 1px !important;
    }
</style>

<ul class="nav nav-pills nav-pills-info" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#link1" role="tablist" aria-expanded="true">
            List User
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#link2" role="tablist" aria-expanded="false">
            New User
        </a>
    </li>
    <li class="nav-item" id="id_User_tab">
        <a class="nav-link" data-toggle="tab" href="#link4" role="tablist" aria-expanded="false">
            Edit User
        </a>
    </li>
    <li class="nav-item" hidden="">
        <a class="nav-link" data-toggle="tab" href="#link3" role="tablist" aria-expanded="false">
            Report
        </a>
    </li>
</ul>
