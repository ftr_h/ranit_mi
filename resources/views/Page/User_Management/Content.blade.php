<div class="tab-content tab-space">
    <div class="tab-pane active" id="link1" aria-expanded="true">
        
    </div>
    <div class="tab-pane" id="link2" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post" id="form-add-user">
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">Add User
                  </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input type="text" class="form-control" value="" name="name" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <select name="role" required id="role" class="selectpicker col-md-12 user_role" data-style="select-with-transition">
                                
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="text" class="form-control" value="" name="username" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" class="form-control" value="" name="email" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control" value="" name="password" required="">
                        </div>
                      </div>
                    </div>
                    <div class="user-form">
                      
                    </div>
                    <div class="row">
                      <br>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="bt_add_user">Save</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="tab-pane" id="link4" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post" id="form-edit-user">
{{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">

                  </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input type="text" class="form-control" value="" name="name" id="edit_name" required="">
                          <input type="hidden" class="form-control" name="id" value="" id="edit_id">
                          <!-- <input type="hidden" class="form-control" name="privilege" value="2" name="privilege"> -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <select name="role" required id="edit_role" class="selectpicker col-md-12 user_role" data-style="select-with-transition">
                               
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Username</label>
                          <input type="text" class="form-control datepicker" value="" name="username" required id="edit_username">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Email</label>
                          <input type="text" class="form-control datepicker" value="" name="email" required id="edit_email">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control" value="" name="password" required="">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="bt_edit_user">Update</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="tab-pane" id="link3" aria-expanded="false">

    </div>
</div>
  <div class="card">
    <div class="card-header card-header-info card-header-icon">
      <div class="card-icon">
        <i class="material-icons">assignment</i>
      </div>
      <h4 class="card-title">List User <!-- 
        <a href="{{asset('admin/master/user/add')}}" id='btnAdd' class="btn btn-sm btn-primary pull-right" title="Add Order" >
            <i class="fa fa-plus-circle"></i> Add User
        </a> -->
      </h4>
    </div>
    <div class="card-body">
      <div class="toolbar">
        <!--        Here you can write extra buttons/actions for the toolbar              -->
      </div>
      <div class="material-datatables">
        <table id="table_user" class="table datatables" cellspacing="0" width="100%" style="width:100%">
            <thead class="text-center">
                <tr>
                    <th width="30">#</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th width="15%" align="right">Action</th>
                </tr>
            </thead>            
        </table>
      </div>
    </div>
    <!-- end content-->
  </div>
