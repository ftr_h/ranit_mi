<!-- Modal -->
  <div class="modal fade" id="modal-list_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-title-list_product">Tambah Produk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-product" style="">
          <form class="form" id="form-product" enctype="multipart/form-data">
            <div class="form-row">
              <div class="form-group col-md-5 text-center" style="min-height: 150px; margin-bottom: 5vh">
                <label>Gambar</label>
                <img onclick="$('#product-image-file').click()" id="product-image" onmouseover="" src="" onerror="this.src = '{{asset('assets/img/add_image.png')}}'" style="cursor:pointer;opacity:0.5; top: 2em;width: 85%; height: 100%; position: absolute; left: 2em;">
                <input type="file" id="product-image-file" name="photo" style="display:none">
              </div>
              <div class="col-md-7">
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="">Nama</label>
                    @csrf
                    <input type="text" class="form-control" name="table" value="ms_product" style="display:none">
                    <input type="text" class="form-control" name="records" value="yes" style="display:none">

                    <input required type="text" class="form-control" name="name" placeholder="Product Name">
                  </div>
                </div>
                
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="">Kategori</label>
                    <select required id="category" name="id_category" class="form-control">
                      
                    </select>
                  </div>
                </div>    
                
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="">Harga / Unit</label>
                    <input required type="number" class="form-control" value="0" name="price" placeholder="Inventory Price per Unit">
                  </div>
                </div>

              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-close" data-dismiss="modal">Tutup</button>
          <button type="submit" form="form-product" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $.post('get_json', {
        table: 'ms_category'
      }, function(res, textStatus){
        
        for(var i = 0; i < res.data.length ; i++){
          $('#category').append('<option value="'+res.data[i].id+'">'+res.data[i].name+'</option>');
        }
      }, 'JSON');

      $("#product-image-file").change(function() {
        readURL(this, "product-image");
      });
    });
  </script>