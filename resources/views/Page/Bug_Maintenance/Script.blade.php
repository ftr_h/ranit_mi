<script>
$(document).ready(function() {
  $('#id_Bug_tab').hide();
  $('#table').DataTable({
      responsive: true,
      info: false
  });
 $.ajax({
    url    : "admin/ticket",
    method : "GET",
    type   : "json",
    success: function (data) {      
        console.log(data);
        // var t = $('#table_project').DataTable();
        // t.row.add( [
        //     counter +'.1',
        //     counter +'.2',
        //     counter +'.3',
        //     counter +'.4',
        //     counter +'.5'
        // ] ).draw( false );
        // $('#edit_liter').val(data.ajax_po.liter)
        // alert(data.page_title);
        var priority_ticket = `<option value="">-- Priority --</option>`;
        $.each(data.priority,function(index,StObj){
            priority_ticket += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.priority_ticket').html(priority_ticket).selectpicker('refresh');

        var project_ticket = `<option value="">-- Project --</option>`;
        $.each(data.project,function(index,StObj){
            project_ticket += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.project_ticket').html(project_ticket).selectpicker('refresh');
    }  
  })
    var dataTable = $('#table_ticket').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url : 'ticket-datatable',
                method: 'POST',
                data: {
                    "table"     : "ms_project"
                },
                global:false,
                async:false
            },
            
            columns: [
                { data: 'id', name: 'id'},
                { data: 'code_ticket', name: 'code_ticket'},
                { data: 'code_project', name: 'code_project'},
                { data: 'project_name', name: 'project_name'},
                { data: 'created_at', name: 'created_at'},
                { data: 'deadline', name: 'deadline'},
                { data: 'priority_name', name: 'priority_name'},
                { data: 'status_name', name: 'status_name'},
                { data: 'id',className: "text-right", render: function (data, type, row, meta) {
                        return `
                        <button onclick="get_data(`+data+`)" type="button" class="btn btn-link btn-just-icon btn-primary btn-edit" data-original-title="Fix" title="Edit Data">
                            <i class="material-icons">fact_check</i>
                        </button>`;
                    }
                },

            ]
        });
  
} );
function fix_bug(id, id_ticket) {
    // alert(id_ticket);
    $.ajax({
        url    : "admin/ticket/bug/fix/"+id,
        method : "GET",
        type   : "json",
        success: function (data) {      
            console.log(data);
            if (data.value == 1) {

                show_notif("success", data.message);
                get_data(id_ticket);
            } else {
                show_notif("danger", data.message);

            }
          
          // $('#edit_liter').val(data.ajax_po.liter)      
        }  
    })
}
function get_data(id) {
$('#myTab li:nth-child(2) a').tab('show') // Select third tab
$('#id_Bug_tab').show();
 $.ajax({
    url    : "{{route('ajaxEditTicket')}}",
    method : "GET",
    data   : {
                id : id
            },
    type   : "json",
    success: function (data) {      
        console.log(data)
      
      var base_url = window.location.origin + '/cms_nps/';
      $('#edit_code_ticket').text(data.ajax_ticket.code_ticket);
      $('#edit_tester_name').text(data.ajax_ticket.tester_name);
      $('#edit_id').val(data.ajax_ticket.id);
      $('#edit_code_ticket_h').val(data.ajax_ticket.code_ticket);
      // $('#edit_status').selectpicker('val', data.ajax_ticket.status);
      $('#edit_project').selectpicker('val', data.ajax_ticket.id_project);
      $('#edit_priority').selectpicker('val', data.ajax_ticket.priority);
      $('#edit_deadline').val(data.ajax_ticket.deadline).trigger("change");
      $('.selectpicker').selectpicker();
      
        var bug_row = ``
        $.each(data.detail,function(index,lkObj){
          // alert(lkObj.bug_desc);
            bug_row += `
                <tr id="edit_cell${lkObj.id}"><td class="text-center"><img id="edit_product-image${lkObj.id}" onmouseover="" src="{{asset('${lkObj.bug_img}')}}" onerror="this.src = {{asset('assets/img/add_image.png')}}" style="cursor:pointer; top: 2em; height: 100px;">
                    <input type="file" id="edit_product-image-file${lkObj.id}" onchange="readURL(this, 'edit_product-image${lkObj.id}');" name="edit_photo${lkObj.id}" style="display:none"></td>
                    <td>
                    <input type="hidden" name="edit_detail_id[]" id="edit_detail_id${lkObj.id}" value="${lkObj.id}"/>
                    ${lkObj.bug_desc}
                    </td>
                    <td>
                        ${lkObj.priority_name}
                    </td>
                    <td>`;
            if (lkObj.status == 1) {
                bug_row +=`
                            <button id="edit_close${lkObj.id}" type="button" class="btn btn-link btn-just-icon btn-success btn-delete" onclick="fix_bug(${lkObj.id}, ${data.ajax_ticket.id})"><i class="fa fa-check"></i></button>`;
            } else {
                bug_row +=`
                            <button id="edit_close${lkObj.id}" type="button" class="btn btn-link btn-just-icon btn-primary btn-delete" onclick="alert('fixed')"><i class="fa fa-eye"></i></button>`;
            }
            bug_row +=` 
                    </td>
                </tr>`;

        $('#edit_countTypeBug').val(lkObj.id).trigger("change");
        // $('#edit_priority_d'+lkObj.id).selectpicker('val', lkObj.priority);
        });
        $('#edit_bug_row').html(bug_row);

        $.each(data.detail,function(index,lkObj){
          $('#edit_priority_d'+lkObj.id).selectpicker('val', lkObj.priority);
        });
        $('.selectpicker').selectpicker();
      // $('#edit_liter').val(data.ajax_po.liter)      
    }  
  })
}
</script>
