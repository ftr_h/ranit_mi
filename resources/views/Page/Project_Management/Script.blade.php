<style>
    .panel{
        
    }
    .small-box{
        background-color: #1f4e79;
        color: aliceblue;
    
    }
    
    .col-md-2{
    background:#1f4e79;
    color:#FFF;
    margin-bottom: 1%;
    border-radius: 7.5%;      
    }
    .col-half-offset{
        margin-left:2.7%
    
    }
    .small-box p{
        font-size: 11px !important;
    }
    .table-responsive{
        min-height: .01%;
        overflow-x: hidden !important;
    }
    
    @media only screen and (max-width: 600px){
        .table-responsive{
            min-height: .01%;
            overflow-x: scroll !important;
        }
    }
    
    @media only screen and (max-width: 30px){
        .col-xs-5{
            width: 44.666667%;
        }
    }
    
</style>
<script>
$(document).ready(function() {
    $('#id_Project_tab').hide();
    $('#table').DataTable({
        responsive: true,
        info: false
    });
    $('.datepicker-dmy').datetimepicker({
        format:'DD-MM-YYYY'
    })
    // $('.selectpicker').selectpicker();


 $.ajax({
    url    : "admin/project",
    method : "GET",
    type   : "json",
    success: function (data) {      
        console.log(data);
        // alert(data.page_title);
        var status_project = `<option value="">-- Status --</option>`;
        $.each(data.project_status,function(index,StObj){
            status_project += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.project_status').html(status_project).selectpicker('refresh');
    }  
  })

    var dataTable = $('#table_project').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url : 'project-datatable',
                method: 'POST',
                data: {
                    "table"     : "ms_project"
                },
                global:false,
                async:false
            },
            
            columns: [
                { data: 'id', name: 'id'},
                { data: 'code_project', name: 'code_project'},
                { data: 'name', name: 'name'},
                { data: 'start_date', name: 'start_date'},
                { data: 'deadline', name: 'deadline'},
                { data: 'status_name', name: 'status_name'},
                { data: 'id', render: function (data, type, row, meta) {
                        return `
                        <button onclick="get_data(`+data+`)" type="button" class="btn btn-link btn-just-icon btn-primary btn-edit" data-original-title="Edit Data" title="Edit Data">
                            <i class="material-icons">edit</i>
                        </button>
                        <a class="btn btn-link btn-just-icon btn-danger btn-delete" title="Delete" href="javascript:void(0);" onclick="swal({   
                        title: 'Are you sure to delete this Project ?',   
                        // text: 'You will not be able to recover this record data!',   
                        type: 'warning',   
                            showCancelButton: true,   
                            confirmButtonText: 'Yes!', 
                            confirmButtonClass: 'btn btn-success',
                            cancelButtonClass: 'btn btn-danger',
                            cancelButtonText: 'No',  
                            closeOnConfirm: false,
                            buttonsStyling: false
                        }).then (function (result) {

                        if (result.hasOwnProperty('dismiss')) {
                          return;
                        }else{
                          delete_project(${data});
                        }

                        });"><i class="fa fa-trash"></i></a>`;
                    }
                },

            ]
        });
  
} );

$('#bt_add_project').click(function(){
    $('#form-add-project').ajaxSubmit({
        url    : "admin/project/add/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "ProjectManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
        }
    });
});

$('#bt_edit_project').click(function(){
    $('#form-edit-project').ajaxSubmit({
        url    : "admin/project/edit/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "ProjectManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
        }
    });
});
function delete_project(id) {
    
    $.ajax({
        url    : "admin/project/delete/"+id,
        method : "GET",
        type   : "json",
        success: function (data) {      
            console.log(data);
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "ProjectManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
          
          // $('#edit_liter').val(data.ajax_po.liter)      
        }  
    })
}
function get_data(id) {
$('#myTab li:nth-child(3) a').tab('show') // Select third tab
$('#id_Project_tab').show();
 $.ajax({
    url    : "{{route('ajaxEditProject')}}",
    method : "GET",
    data   : {
                id : id
            },
    type   : "json",
    success: function (data) {      
      console.log(data);
      var deadline = data.ajax_project.deadline.split("-").reverse().join("-");
      var start_date = data.ajax_project.start_date.split("-").reverse().join("-");
      $('#edit_code_project').text(data.ajax_project.code_project);
      $('#edit_pm_name').text(data.ajax_project.pm_name);
      $('#edit_id').val(data.ajax_project.id);
      $('#edit_name').val(data.ajax_project.name).trigger("change");
      $('#edit_status').selectpicker();
      $('#edit_status').selectpicker('val', data.ajax_project.status);
      $('#edit_start_date').val(start_date).trigger("change");
      $('#edit_deadline').val(deadline).trigger("change");
      
      // $('#edit_liter').val(data.ajax_po.liter)      
    }  
  })
}
</script>
