<div class="tab-content tab-space">
    <div class="tab-pane active" id="link1" aria-expanded="true">
        
    </div>
    <div class="tab-pane" id="link2" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post" id="form-add-project">
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">Add Project
                  </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Project Name</label>
                          <input type="text" class="form-control" value="" name="name" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <select name="status" required id="status" class="selectpicker col-md-12 project_status" data-style="select-with-transition">
                                
                                
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Start Date</label>
                          <input type="text" class="form-control datepicker-dmy" value="{{date('d-m-Y')}}" name="start_date" required id="start_date">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Deadline</label>
                          <input type="text" class="form-control datepicker-dmy" value="{{date('d-m-Y')}}" name="deadline" required id="deadline">
                        </div>
                      </div>
                    </div>
                    <div class="user-form">
                      
                    </div>
                    <div class="row">
                      <br>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="bt_add_project">Save</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="tab-pane" id="link4" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post" id="form-edit-project">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">
                    <div class="row">
                      <div class="col-6" style="padding: 0;">
                        Project Code : <label for="" class="label label-primary" id="edit_code_project" style="font-size: 1em; font-weight: bolder"></label>
                      </div>
                      <div class="col-6" style="padding: 0; text-align: right;">
                        PM : <label for="" class="label label-primary" id="edit_pm_name" style="font-size: 1em; font-weight: bolder">Nama</label>
                      </div>
                    </div>

                  </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Project Name</label>
                          <input type="text" class="form-control" value="" name="name" id="edit_name" required="">
                          <input type="hidden" class="form-control" name="id" value="" id="edit_id">
                          <!-- <input type="hidden" class="form-control" name="privilege" value="2" name="privilege"> -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <select name="status" required id="edit_status" class="selectpicker col-md-12 project_status" data-style="select-with-transition">
                                
                               
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Start Date</label>
                          <input type="text" class="form-control datepicker-dmy" value="" name="start_date" required id="edit_start_date">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="inputState">Deadline</label>
                          <input type="text" class="form-control datepicker-dmy" value="" name="deadline" required id="edit_deadline">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="bt_edit_project">Update</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="tab-pane" id="link3" aria-expanded="false">

    </div>
</div>
  <div class="card">
    <div class="card-header card-header-info card-header-icon">
      <div class="card-icon">
        <i class="material-icons">assignment</i>
      </div>
      <h4 class="card-title">List Project <!-- 
        <a href="{{asset('admin/master/user/add')}}" id='btnAdd' class="btn btn-sm btn-primary pull-right" title="Add Order" >
            <i class="fa fa-plus-circle"></i> Add Project
        </a> -->
      </h4>
    </div>
    <div class="card-body">
      <div class="toolbar">
        <!--        Here you can write extra buttons/actions for the toolbar              -->
      </div>
      <div class="material-datatables">
        <table id="table_project" class="table datatables" cellspacing="0" width="100%" style="width:100%">
            <thead class="text-center">
                <tr>
                    <th width="30">#</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Start Date</th>
                    <th>Deadline</th>
                    <th>Status</th>
                    <th width="15%" align="right">Action</th>
                </tr>
            </thead>            
        </table>
      </div>
    </div>
    <!-- end content-->
  </div>
