<div class="tab-content tab-space">
    <div class="tab-pane active" id="link1" aria-expanded="true">
      
    </div>
    <div class="tab-pane" id="link2" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post" id="form-add-ticket">
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">Add Ticket
                  </h4>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select name="project" id="project" required class="selectpicker col-md-12 project_ticket" data-style="select-with-transition">
                                  
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select name="priority" id="priority" required class="selectpicker col-md-12 priority_ticket" data-style="select-with-transition">
                                  
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="inputState">Deadline</label>
                            <input type="text" class="form-control datepicker-dmyh" value="{{date('d-m-Y H:i:s')}}" name="deadline" required id="deadline">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9">
                      <div class="material-datatables">
                        <table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>Image/Video</th>
                                    <th>Bug Description</th>
                                    <th>Priority</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody id="bug_row">

                            </tbody>
                              <tr>
                                <td colspan="4">
                                  <div id="btn-paket" class="btn btn-success btn-block" onclick="addBug()">
                                    Tambah
                                  </div>
                                </td>
                              </tr>
                        </table>
                        <input type="hidden" name="typeBug" id="countTypeBug" value="0">
                      </div>
                    </div>
                  </div>
                  <div class="user-form">
                    
                  </div>
                  <div class="row">
                    <br>
                  </div>
                  <button type="button" id="bt_add_ticket" class="btn btn-info pull-right">Save</button>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
<div class="tab-pane" id="link4" aria-expanded="false">
        <form autocomplete='off' enctype="multipart/form-data" method="post"id="form-edit-ticket">
{{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-icon card-header-info">
                  <div class="card-icon">
                    <i class="material-icons">perm_identity</i>
                  </div>
                  <h4 class="card-title">
                    <div class="row">
                      <div class="col-6" style="padding: 0;">
                        Ticket Code : <label for="" class="label label-primary" id="edit_code_ticket" style="font-size: 1em; font-weight: bolder"></label>
                      </div>
                      <div class="col-6" style="padding: 0; text-align: right;">
                        Reported By : <label for="" class="label label-primary" id="edit_tester_name" style="font-size: 1em; font-weight: bolder">Nama</label>
                      </div>
                    </div>

                  </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="hidden" class="form-control" name="id" value="" id="edit_id">
                              <input type="hidden" class="form-control" name="code_ticket" value="" id="edit_code_ticket_h">
                              <select name="project" id="edit_project" required class="selectpicker col-md-12 project_ticket" data-style="select-with-transition" required="">
                                    
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <select name="priority" id="edit_priority" required class="selectpicker col-md-12 priority_ticket" data-style="select-with-transition" required="">
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="inputState">Deadline</label>
                              <input type="text" class="form-control datepicker-dmyh" value="" name="deadline" required id="edit_deadline" required="">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="material-datatables">
                          <table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                              <thead class="text-center">
                                  <tr>
                                      <th>Image/Video</th>
                                      <th>Bug Description</th>
                                      <th>Priority</th>
                                      <th width="10%">Action</th>
                                  </tr>
                              </thead>
                              <tbody id="edit_bug_row">

                              </tbody>
                                <tr>
                                  <td colspan="4">
                                    <div id="btn-paket" class="btn btn-success btn-block" onclick="edit_addBug()">
                                      Tambah
                                    </div>
                                  </td>
                                </tr>
                          </table>
                          <input type="hidden" name="typeBug" id="edit_countTypeBug" value="0">
                        </div>
                      </div>
                    </div>
                    <button type="button" id="bt_edit_ticket" class="btn btn-info pull-right">Update</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="tab-pane" id="link3" aria-expanded="false">

    </div>
</div>
  <div class="card">
    <div class="card-header card-header-info card-header-icon">
      <div class="card-icon">
        <i class="material-icons">assignment</i>
      </div>
      <h4 class="card-title">List Ticket <!-- 
        <a href="{{asset('admin/master/user/add')}}" id='btnAdd' class="btn btn-sm btn-primary pull-right" title="Add Order" >
            <i class="fa fa-plus-circle"></i> Add Ticket
        </a> -->
      </h4>
    </div>
    <div class="card-body">
      <div class="toolbar">
        <!--        Here you can write extra buttons/actions for the toolbar              -->
      </div>
      <div class="material-datatables">
        <table id="table_ticket" class="table datatables" cellspacing="0" width="100%" style="width:100%">
            <thead class="text-center">
                <tr>
                    <th width="30">#</th>
                    <th>Ticket Code</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Reported at</th>
                    <th>Deadline</th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th width="15%" align="right">Action</th>
                </tr>
            </thead>
        </table>
      </div>
    </div>
    <!-- end content-->
  </div>
