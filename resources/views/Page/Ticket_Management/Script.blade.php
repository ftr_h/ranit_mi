<script>
$(document).ready(function() {
  $('#id_Ticket_tab').hide();
  $('#table').DataTable({
      responsive: true,
      info: false
  });

  $('.datepicker-dmyh').datetimepicker({
      format:'DD-MM-YYYY HH:mm:ss'
  })
  $('.datetimepicker').datetimepicker();
  $('.datepicker').datetimepicker({
      format:'YYYY-MM-DD'
  })
 $.ajax({
    url    : "admin/ticket",
    method : "GET",
    type   : "json",
    success: function (data) {      
        console.log(data);
        // var t = $('#table_project').DataTable();
        // t.row.add( [
        //     counter +'.1',
        //     counter +'.2',
        //     counter +'.3',
        //     counter +'.4',
        //     counter +'.5'
        // ] ).draw( false );
        // $('#edit_liter').val(data.ajax_po.liter)
        // alert(data.page_title);
        var priority_ticket = `<option value="">-- Priority --</option>`;
        $.each(data.priority,function(index,StObj){
            priority_ticket += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.priority_ticket').html(priority_ticket).selectpicker('refresh');

        var project_ticket = `<option value="">-- Project --</option>`;
        $.each(data.project,function(index,StObj){
            project_ticket += `<option value="${StObj.id}">${StObj.name}</option>`;
        });
        $('.project_ticket').html(project_ticket).selectpicker('refresh');
    }  
  })
    var dataTable = $('#table_ticket').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url : 'ticket-datatable',
                method: 'POST',
                data: {
                    "table"     : "ms_project"
                },
                global:false,
                async:false
            },
            
            columns: [
                { data: 'id', name: 'id'},
                { data: 'code_ticket', name: 'code_ticket'},
                { data: 'code_project', name: 'code_project'},
                { data: 'project_name', name: 'project_name'},
                { data: 'created_at', name: 'created_at'},
                { data: 'deadline', name: 'deadline'},
                { data: 'priority_name', name: 'priority_name'},
                { data: 'status_name', name: 'status_name'},
                { data: 'id', render: function (data, type, row, meta) {
                        return `
                        <button onclick="get_data(`+data+`)" type="button" class="btn btn-link btn-just-icon btn-primary btn-edit" data-original-title="Edit Data" title="Edit Data">
                            <i class="material-icons">edit</i>
                        </button>
                        <a class="btn btn-link btn-just-icon btn-danger btn-delete" title="Delete" href="javascript:void(0);" onclick="swal({   
                        title: 'Are you sure to delete this Ticket ?',   
                        // text: 'You will not be able to recover this record data!',   
                        type: 'warning',   
                            showCancelButton: true,   
                            confirmButtonText: 'Yes!', 
                            confirmButtonClass: 'btn btn-success',
                            cancelButtonClass: 'btn btn-danger',
                            cancelButtonText: 'No',  
                            closeOnConfirm: false,
                            buttonsStyling: false
                        }).then (function (result) {

                        if (result.hasOwnProperty('dismiss')) {
                          return;
                        }else{
                          delete_ticket(${data});
                        }

                        });"><i class="fa fa-trash"></i></a>`;
                    }
                },

            ]
        });
  
} );
$('#bt_add_ticket').click(function(){
    $('#form-add-ticket').ajaxSubmit({
        url    : "admin/ticket/add/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            // alert("okokok");
            if (data.value == 1) {

                show_notif("success", data.message);
                $.ajax({
                    url: "{{route('MenuMaster')}}",
                    data: {
                      redirect : "TicketManagement"
                    },
                    method : 'POST',
                    type : "POST",
                    success: function(e){
                      $('.content').html(e);
                    },
                    error: function(e){
                      console.log(e);
                    }

                });
            } else {
                show_notif("danger", data.message);

            }
        }
    });
});
$('#bt_edit_ticket').click(function(){
    $('#form-edit-ticket').ajaxSubmit({
        url    : "admin/ticket/edit/save",
        method : "POST",
        type   : "json",
        success: function (data) {      
            console.log(data);
            // alert("okokok");

            show_notif("success", "Berhasil ubah data");
            $.ajax({
                url: "{{route('MenuMaster')}}",
                data: {
                  redirect : "TicketManagement"
                },
                method : 'POST',
                type : "POST",
                success: function(e){
                  $('.content').html(e);
                },
                error: function(e){
                  console.log(e);
                }

            });
            // if (data.value == 1) {

            // } else {
            //     show_notif("danger", data.message);

            // }
        }
    });
});

// function delete_ticket(id) {
    
//     $.ajax({
//         url    : "admin/ticket/delete/"+id,
//         method : "GET",
//         type   : "json",
//         success: function (data) {      
//             console.log(data);
//             if (data.value == 1) {

//                 show_notif("success", data.message);
//                 $.ajax({
//                     url: "{{route('MenuMaster')}}",
//                     data: {
//                       redirect : "ProjectManagement"
//                     },
//                     method : 'POST',
//                     type : "POST",
//                     success: function(e){
//                       $('.content').html(e);
//                     },
//                     error: function(e){
//                       console.log(e);
//                     }

//                 });
//             } else {
//                 show_notif("danger", data.message);

//             }
          
//           // $('#edit_liter').val(data.ajax_po.liter)      
//         }  
//     })
}
function get_data(id) {
$('#myTab li:nth-child(3) a').tab('show') // Select third tab
$('#id_Ticket_tab').show();
 $.ajax({
    url    : "{{route('ajaxEditTicket')}}",
    method : "GET",
    data   : {
                id : id
            },
    type   : "json",
    success: function (data) {      
      var deadline = new Date(data.ajax_ticket.deadline);
      // alert(deadline);
      var base_url = window.location.origin + '/cms_nps/';
      $('#edit_code_ticket').text(data.ajax_ticket.code_ticket);
      $('#edit_tester_name').text(data.ajax_ticket.tester_name);
      $('#edit_id').val(data.ajax_ticket.id);
      $('#edit_code_ticket_h').val(data.ajax_ticket.code_ticket);
      // $('#edit_status').selectpicker('val', data.ajax_ticket.status);
      $('#edit_project').selectpicker('val', data.ajax_ticket.id_project);
      $('#edit_priority').selectpicker('val', data.ajax_ticket.priority);
      $('#edit_deadline').val(data.ajax_ticket.deadline).trigger("change");
      // $("#edit_deadline").datetimepicker("setDate", deadline);
      // $('#edit_deadline').pickadate('picker').set('select', data.ajax_ticket.deadline);
      $('.selectpicker').selectpicker();
      // $('.datetimepicker').datetimepicker();
      // $('.datetimepicker').datetimepicker({
      //   date: new Date()
      // });
      
        var bug_row = ``
        $.each(data.detail,function(index,lkObj){
          // alert(lkObj.bug_desc);
          bug_row += `
            <tr id="edit_cell${lkObj.id}"><td class="text-center"><img onclick="$('#edit_product-image-file${lkObj.id}').click()" id="edit_product-image${lkObj.id}" onmouseover="" src="{{asset('${lkObj.bug_img}')}}" onerror="this.src = {{asset('assets/img/add_image.png')}}" style="cursor:pointer; top: 2em; height: 100px;">
                <input type="file" id="edit_product-image-file${lkObj.id}" onchange="readURL(this, 'edit_product-image${lkObj.id}');" name="edit_photo${lkObj.id}" style="display:none"></td>
                <td>
                <input type="hidden" name="edit_detail_id[]" id="edit_detail_id${lkObj.id}" value="${lkObj.id}"/>
                    <textarea class="form-control" id="edit_bug_desc${lkObj.id}" required rows="3" name="edit_bug_desc[]" value="">${lkObj.bug_desc}</textarea>
                </td>
                <td>
                    <select name="edit_priority_d[]" id="edit_priority_d${lkObj.id}" required class="selectpicker col-md-12" data-style="select-with-transition">
                        <option value="">-- Priority --</option>`;
                        $.each(data.priority,function(index1,prObj){
                          bug_row += `<option value="${prObj.id}">${prObj.name}</option>`;
                        });
                bug_row += `
                    </select>
                </td>
                <td>
                    <button id="edit_close${lkObj.id}" type="button" class="btn btn-link btn-just-icon btn-danger btn-delete" onclick="hapusBug(${lkObj.id})"><i class="fa fa-close"></i></button>
                </td>
            </tr>`;

        $('#edit_countTypeBug').val(lkObj.id).trigger("change");
        // $('#edit_priority_d'+lkObj.id).selectpicker('val', lkObj.priority);
        });
        $('#edit_bug_row').html(bug_row);

        $.each(data.detail,function(index,lkObj){
          $('#edit_priority_d'+lkObj.id).selectpicker('val', lkObj.priority);
        });
        $('.selectpicker').selectpicker();
      // $('#edit_liter').val(data.ajax_po.liter)      
    }  
  })
}
  function addBug(){
    var next = parseInt($("#countTypeBug").val())+1;
    $.ajax({
      type: "GET",
      url  : "{{ route('ajaxGetNewBug') }}",
      data: {next: next},
      success: function(data) {
        $('.selectpicker').selectpicker();
        $("#bug_row").append(data);
        $("#countTypeBug").val(next);
        $('.selectpicker').selectpicker();
      }
    });
  }
  function edit_addBug(){
    var next = parseInt($("#edit_countTypeBug").val())+1;
    $.ajax({
      type: "GET",
      url  : "{{ route('ajaxGetEditNewBug') }}",
      data: {next: next},
      success: function(data) {
        $("#edit_bug_row").append(data);
        $("#edit_countTypeBug").val(next);
        $('.selectpicker').selectpicker();
      }
    });
  }
  function removeCell(id) {
    var to = parseInt($("#countTypeBug").val())-1;
    //$("#countTypeBug").val(to);
    $("#cell"+id).remove();
  }
  function editNewremoveCell(id) {
    var to = parseInt($("#edit_countTypeBug").val())-1;
    //$("#edit_countTypeBug").val(to);
    $("#edit_cell"+id).remove();
  }
  function hapusBug(id){
    $.ajax({
      type: "GET",
      url  : "{{ route('ajaxHapusBug') }}",
      data: {id: id},
      success: function(data) {
       if (data == 'sukses') {
        editremoveCell(id);
       } 
      }
    });
  }
  function editremoveCell(id) {
    var to = parseInt($("#edit_countTypeLokasi").val())-1;
    //$("#edit_countTypeLokasi").val(to);
    $("#edit_cell"+id).remove();
  }
  $("#product-image-file").change(function() {
    readURL(this, "product-image");
  });
  function changeImg(input,next){
    readURL(input, "product-image-file".next);
    // alert($(input).attr(`id`));
  }
  function readURL(input, target_image) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#'+target_image).attr('src', e.target.result).show();
        $('#'+target_image).css('opacity','1');
      }

      reader.readAsDataURL(input.files[0]);
    }else{
      $('#'+target_image).css('opacity','0.5');
    }
  } 
</script>
