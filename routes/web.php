<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MenuMaster@checkSession')->name('MenuLogin');

Route::post('MenuMaster', 'MenuMaster@index')->name('MenuMaster');

//login
Route::get('login', 'AdminLoginController@get_login')->name('login')->middleware('logged.in.cek');
Route::post('post-login', 'AdminLoginController@postLogin')->name('postLogin');
Route::post('gen-password', 'AdminLoginController@genPassword')->name('genPassword');
Route::get('logout', 'AdminLoginController@logout')->name('logout');

Route::middleware('access_permission')->group(function(){
	//dashboard
	Route::get('dashboard', 'DashboardController@getDashboard')->name('dashboard');

	//project
	Route::get('admin/project/','AdminProjectController@getProjectManagement')->name('getProjectManagement');
	Route::get('admin/project/edit/{id}','AdminProjectController@getEditProject')->name('getEditProject');
	Route::post('admin/project/add/save/','AdminProjectController@saveAddProject')->name('saveAddProject');
	Route::post('admin/project/edit/save/','AdminProjectController@editProject')->name('editProject');
	Route::get('ajax-edit-project','AjaxController@ajaxEditProject')->name('ajaxEditProject');
	Route::get('admin/project/delete/{id}','AdminProjectController@deleteProject')->name('deleteProject');
	Route::post('project-datatable', 'AdminProjectController@projectDatatable');

	//user
	Route::get('admin/user/','AdminUserController@getUserManagement')->name('getUserManagement');
	Route::get('admin/user/edit/{id}','AdminUserController@getEditUser')->name('getEditUser');
	Route::post('admin/user/add/save/','AdminUserController@saveAddUser')->name('saveAddUser');
	Route::post('admin/user/edit/save/','AdminUserController@editUser')->name('editUser');
	Route::get('ajax-edit-user','AjaxController@ajaxEditUser')->name('ajaxEditUser');
	Route::get('admin/user/delete/{id}','AdminUserController@deleteUser')->name('deleteUser');
	Route::post('user-datatable', 'AdminUserController@userDatatable');

	//ticket
	Route::get('admin/ticket','AdminTicketController@getTicketManagement')->name('getTicketManagement');
	Route::get('admin/ticket/edit/{id}','AdminTicketController@getEditTicket')->name('getEditTicket');
	Route::post('admin/ticket/add/save/','AdminTicketController@saveAddTicket')->name('saveAddTicket');
	Route::post('admin/ticket/edit/save/','AdminTicketController@editTicket')->name('editTicket');
	Route::get('ajax-edit-ticket','AjaxController@ajaxEditTicket')->name('ajaxEditTicket');
	Route::get('admin/ticket/delete/{id}','AdminTicketController@deleteTicket')->name('deleteTicket');
	Route::get('admin/ticket/bug/fix/{id}','AdminTicketController@fixBug')->name('fixBug');
	Route::get('ajax-new-bug','AjaxController@ajaxGetNewBug')->name('ajaxGetNewBug');
	Route::get('ajax-edit-new-bug','AjaxController@ajaxGetEditNewBug')->name('ajaxGetEditNewBug');
	Route::get('ajax-hapus-bug','AjaxController@ajaxHapusBug')->name('ajaxHapusBug');
	Route::post('ticket-datatable', 'AdminTicketController@ticketDatatable');

	//profile
	Route::get('admin/master/user/profile','AdminUsersCompanyController@getProfile')->name('getProfile');
	Route::post('admin/master/user/edit/save/','AdminUsersCompanyController@saveEditUser')->name('saveEditUser');
	Route::post('admin/users_company/user/edit/save/','AdminUsersCompanyController@saveEditUser')->name('saveEditUser');


	Route::middleware('cek_akses_admin')->group(function(){

	Route::get('transactions/advance/update','AdminTransactionsController@updateAdvance')->name('updateAdvance');

	});
	//user-profile
	Route::get('user-profil','UserProfileController@user_profil')->name('user_profil');
	Route::post('save-profil','UserProfileController@save_profile')->name('save_profile');

	//ajax
	Route::get('ajax-city', 'AjaxController@ajaxCity')->name('ajaxCity');

	Route::get('ajax-customera', 'AjaxController@ajaxCustomera')->name('ajaxCustomera');

	Route::get('ajax-customer','AjaxController@ajaxCustomer')->name('ajaxCustomer');
	Route::get('ajax-penawaran','AjaxController@ajaxPenawaran')->name('ajaxPenawaran');
	Route::get('ajax-ref-pay','AjaxController@ajaxRefPay')->name('ajaxRefPay');
	Route::get('ajax-inv','AjaxController@ajaxInvoice')->name('ajaxInvoice');
	Route::get('ajax-edit-inv','AjaxController@ajaxEditInvoice')->name('ajaxEditInvoice');
	Route::get('ajax-edit-po','AjaxController@ajaxEditPO')->name('ajaxEditPO');
	Route::get('ajax-add-po','AjaxController@ajaxAddPO')->name('ajaxAddPO');
	Route::get('ajax-add-inv','AjaxController@ajaxAddInvoice')->name('ajaxAddInvoice');
	Route::get('ajax-edit-customer','AjaxController@ajaxEditCustomer')->name('ajaxEditCustomer');
	Route::get('ajax-edit-penawaran','AjaxController@ajaxEditPenawaran')->name('ajaxEditPenawaran');
	Route::get('ajax-edit-user','AjaxController@ajaxEditUser')->name('ajaxEditUser');
	Route::get('ajax-edit-kapal','AjaxController@ajaxEditKapal')->name('ajaxEditKapal');
	Route::get('ajax-edit-vehicle','AjaxController@ajaxEditVehicle')->name('ajaxEditVehicle');
	Route::get('ajax-t-vehicle','AjaxController@ajaxTVehicle')->name('ajaxTVehicle');
	Route::get('ajax-edit-t-vehicle','AjaxController@ajaxEditTVehicle')->name('ajaxEditTVehicle');
	Route::get('ajax-new-lokasi','AjaxController@ajaxGetNewLokasi')->name('ajaxGetNewLokasi');
	Route::get('ajax-edit-new-lokasi','AjaxController@ajaxGetEditNewLokasi')->name('ajaxGetEditNewLokasi');
	Route::get('ajax-hapus-lokasi','AjaxController@ajaxHapusLokasi')->name('ajaxHapusLokasi');
	Route::get('ajax-company', 'AjaxController@ajaxCompany')->name('ajaxCompany');
	Route::get('ajax-company-new','AjaxController@ajaxCompanyNew')->name('ajaxCompanyNew');
	Route::get('ajax-post', 'AjaxController@ajaxPost')->name('ajaxPost');
	Route::get('ajax-post-edit', 'AjaxController@ajaxPostEdit')->name('ajaxPostEdit');
});
Route::get('chart_order', 'AjaxController@chartOrder')->name('chartOrder');