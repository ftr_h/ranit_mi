<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class MenuMaster_Model
{
    public static function DashboardMenu(){
        $data = [];
        $data['page_title'] = 'DASHBOARD';      
        return $data;
    }

    public static function ProjectManagementMenu(){
       $data = [];
       $data['project_status'] = [];
       $data['page_title'] = 'Project Management';

       if (session('id_role') == 3) {
          $data['result'] = DB::table('ms_project')
                            ->join('ms_project_status', 'ms_project.status', '=', 'ms_project_status.id')
                            ->select('ms_project.*', 'ms_project_status.name as status_name')
                            ->where('id_pm',session('id'))
                            ->orderby('id','desc')
                            ->get();
       } else {
          $data['result'] = DB::table('ms_project')
                            ->join('ms_project_status', 'ms_project.status', '=', 'ms_project_status.id')
                            ->select('ms_project.*', 'ms_project_status.name as status_name')
                            ->orderby('id','desc')
                            ->get();
       }
       $data['project_status'] = DB::table('ms_project_status')->get();
       return $data;
    }

     public static function TicketManagementMenu() {
       $data = [];
       $data['page_title'] = 'Ticket Management';
       $data['result'] = DB::table('t_ticket')
                        ->join('ms_project', 't_ticket.id_project', '=', 'ms_project.id')
                        ->join('ms_ticket_status', 't_ticket.status', '=', 'ms_ticket_status.id')
                        ->join('ms_priority', 't_ticket.priority', '=', 'ms_priority.id')
                        ->select('t_ticket.*', 'ms_ticket_status.name as status_name', 'ms_project.name as project_name', 'ms_project.code_project as code_project', 'ms_priority.name as priority_name', 'ms_priority.type as priority_type')
                        ->orderby('id','desc')
                        ->get();
       $data['project'] = DB::table('ms_project')->get();
       $data['priority'] = DB::table('ms_priority')->get();
       return $data;
    }
}
