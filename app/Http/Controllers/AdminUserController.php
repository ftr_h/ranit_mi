<?php namespace App\Http\Controllers;

  use Session;
  use Alert;
  use PDF;
  use Excel;
  use DB;
  use Redirect;
  use Cache;
  use Image;
  use Route;
  use Schema;
  use Storage;
  use Datatables;

  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Validator;
  use Illuminate\Support\Facades\Input;
  class AdminUserController extends Controller {

    public static function getUserManagement(){
        $data = [];
        $data['user_status'] = [];
        $data['page_title'] = 'User Management';

        $data['result'] = DB::table('ms_user')
                          ->join('ms_role', 'ms_user.id_role', '=', 'ms_role.id')
                          ->select('ms_user.*', 'ms_role.name as role_name')
                          ->orderby('id','desc')
                          ->get();
        $data['role'] = DB::table('ms_role')->get();
        return $data;
    }
    public function userDatatable(Request $request){
      $data = DB::table('ms_user')
                      ->join('ms_role', 'ms_user.id_role', '=', 'ms_role.id')
                      ->select('ms_user.*', 'ms_role.name as role_name')
                      ->orderby('id','desc')
                      ->get();
      return Datatables::of($data)->make(true);
    }
    public function saveAddUser(Request $request)
    {
        $date_now_ymd = date("Y-m-d");
        $password = \Hash::make($request->input('password'));
        // dd($new_id);

        $sql = DB::insert("INSERT INTO ms_user (
                    name,
                    username,
                    email,
                    password,
                    id_role,
                    created_at,
                    updated_at)
                    values (
                        '".$request->input('name')."',
                        '".$request->input('username')."',
                        '".$request->input('email')."',
                        '".$password."',
                        '".$request->input('role')."',
                        '".$date_now_ymd."',
                        '".$date_now_ymd."')");
        if($sql){
            $response["value"] = 1;
            $response["message"] = "Sukses tambah data";
            // echo json_encode($response); //merubah respone menjadi JsonObject
            // return redirect()->route('getUserManagement');
        }else{
            $response["value"] = 0;
            $response["message"] = "Gagal tambah data";
            // echo json_encode($response); //merubah respone menjadi JsonObject
        }
        return $response;
    }
    public function editUser(Request $request)
    {
        // dd($request->all());
        $id = $request->input('id');
        if ($request->input('password') != null) {
            $password = \Hash::make($request->input('password'));
            $sql = DB::update("UPDATE ms_user set 
                            name = '".$request->input('name')."',
                            username = '".$request->input('username')."',
                            email = '".$request->input('email')."',
                            password = '".$password."',
                            id_role = '".$request->input('role')."'
                            where id=".$id);
        } else {
            $sql = DB::update("UPDATE ms_user set 
                            name = '".$request->input('name')."',
                            username = '".$request->input('username')."',
                            email = '".$request->input('email')."',
                            id_role = '".$request->input('role')."'
                            where id=".$id);
        }
        
        if($sql){
            $response["value"] = 1;
            $response["message"] = "Sukses ubah data";
            // echo json_encode($response); //merubah respone menjadi JsonObject
            // return redirect()->route('getUserManagement');
        }else{
            $response["value"] = 0;
            $response["message"] = "Gagal ubah data";
            // echo json_encode($response); //merubah respone menjadi JsonObject
        }
        return $response;
    }
    public function deleteUser($id) {
      //First, Add an auth
      // dd($id);
      
      $deleteUser = DB::table('ms_user')->where('id', $id)->delete();
       //Create a view. Please use `cbView` method instead of view method from laravel.
          
      if($deleteUser){
          $response["value"] = 1;
          $response["message"] = "Sukses hapus data";
          // echo json_encode($response); //merubah respone menjadi JsonObject
          // return redirect()->route('getTicketManagement');
      }else{
          $response["value"] = 0;
          $response["message"] = "Gagal hapus data";
          // echo json_encode($response); //merubah respone menjadi JsonObject
      }
      return $response;
    }

  }