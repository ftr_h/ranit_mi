<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Session;
use Hash;
use App\MenuMaster_Model AS MM;

class MenuMaster extends Controller
{
    public function index(Request $request){
    	$redirect = $request->post('redirect');

    	if($redirect != null){
    		$data = [];

    		switch ($redirect) {
    			case 'Dashboard' :
    				$data = MM::DashboardMenu();
    				return view('Page.dashboard', $data);
    			break;
    			case 'TicketManagement':
    				$data = MM::TicketManagementMenu();
    				return view('Page.index_ticket_management');
    			break;
    			case 'ProjectManagement':
    				$data = MM::ProjectManagementMenu();
    				return view('Page.index_project_management');
    			break;
                case 'UserManagement':
                    return view('Page.index_user_management');
                break;
                case 'BugMaintenance':
                    return view('Page.index_bug_maintenance');
                break;
    		}

    	}
    }

    public function checkSession(){
    	if (Session::get('is_login') == true || Session::get('is_login') != null) {
            return view('master');
        } else if(Session::get('is_login') == false){
            return redirect()->route("login");
        }
    }

    

    
}
